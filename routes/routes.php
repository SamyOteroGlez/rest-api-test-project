<?php

$route->get('/api/all', 'App\Controllers\PhoneBookController::all');
$route->get('/api/all/page={page:[0-9]+}', 'App\Controllers\PhoneBookController::all');

$route->get('/api/find/by/id/{id}', 'App\Controllers\PhoneBookController::findById');

$route->get('/api/find/by/first/name/{firstName}', 'App\Controllers\PhoneBookController::findByFirstName');
$route->get('/api/find/by/first/name/{firstName}/page={page:[0-9]+}', 'App\Controllers\PhoneBookController::findByFirstName');

$route->get('/api/find/by/last/name/{lastName}', 'App\Controllers\PhoneBookController::findByLastName');
$route->get('/api/find/by/last/name/{lastName}/page={page:[0-9]+}', 'App\Controllers\PhoneBookController::findByLastName');

$route->post('/api/create', 'App\Controllers\PhoneBookController::create');
$route->post('/api/update/{id}', 'App\Controllers\PhoneBookController::update');
$route->delete('/api/delete/{id}', 'App\Controllers\PhoneBookController::delete');
