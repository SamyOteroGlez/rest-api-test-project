APP_IMAGE = phonebook
DOCKER_COMPOSE = "docker-compose"

default: up

.PHONY: up
up:
	$(DOCKER_COMPOSE) up -d

.PHONY: down
down:
	$(DOCKER_COMPOSE) down

.PHONY: destroy
destroy:
	$(DOCKER_COMPOSE) down -v --rmi all

.PHONY: rebuild
rebuild: down
	$(DOCKER_COMPOSE) up --build

.PHONY: shell
shell:
	$(DOCKER_COMPOSE) exec $(APP_IMAGE) /bin/sh
