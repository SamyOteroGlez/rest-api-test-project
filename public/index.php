<?php

/**
 * Test project.
 *
 * Title: Phone book.
 * Description: Implementation of a restful API using php packages.
 * Author: Samy Otero.
 */
define('APPLICATION', microtime(true));

/**
 * Register the auto loader of the application.
 */
require __DIR__ . '/../vendor/autoload.php';

/**
 * Bootstrap the application.
 */
$app = require_once __DIR__ . '/../core/bootstrap.php';

/**
 * Handle the requests and responses.
 */
$app->handle();
