<?php

namespace App\Services;

interface ExternalApiCallInterface
{
    /**
     * Get the resource from an external source.
     */
    public function get();
}
