<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use App\Services\ExternalApiCallInterface;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Exception\BadResponseException;

class TimeZoneService implements ExternalApiCallInterface
{
    /**
     * List of time zones;
     *
     * @var array
     */
    protected $timeZones;

    /**
     * External endpoint url.
     *
     * @var string
     */
    protected $url;

    /**
     * Guzzle client object.
     *
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * Cache life time.
     *
     * @var int
     */
    protected $cacheTime;

    /**
     * Class constructor.
     *
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        $this->cacheTime = vars('cache-time');
        $this->url = vars('timezones-endpoint');
        $this->client = $client;
    }

    /**
     * Set time zones array.
     *
     * @param array $timeZones
     * @param boolean $keys
     *
     * @return App\Services\TimeZoneService
     */
    public function setTimeZones($timeZones, $keys = true)
    {
        $this->timeZones = ($keys) ? array_keys($timeZones) : $timeZones;

        return $this;
    }

    /**
     * Get time zones array.
     *
     * @return array
     */
    public function getTimeZones()
    {
        return $this->timeZones;
    }

    /**
     * Get time zones from the external source.
     *
     * @return void
     */
    public function get()
    {
        //check for cached list
        $cached = apcu_fetch('timeZones');

        if (false == $cached) {
            try {
                $response = $this->client->get($this->url);
                $contents = json_decode($response->getBody()->getContents(), true);
                $this->setTimeZones($contents['result']);

                 //cache the list of timezones
                 apcu_store('timeZones', $this->timeZones, $this->cacheTime);
            } catch (ClientException | RequestException | ConnectException |
                        TransferException | BadResponseException | Exception $ex) {
                $class = get_class($ex);

                throw new $class($ex->getMessage(), $ex->getRequest());
            }
        } else {
            $this->timeZones = $cached;
        }

        return $this->timeZones;
    }
}
