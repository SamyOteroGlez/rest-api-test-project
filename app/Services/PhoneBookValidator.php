<?php

namespace App\Services;

use App\Models\BaseModel;
use App\Services\CountryService;
use Respect\Validation\Validator;
use App\Services\TimeZoneService;

class PhoneBookValidator
{
    /**
     * Validator object.
     *
     * @var Respect\Validation\Validator
     */
    protected $validator;

    /**
     * Validations array.
     * If any validation errors happened, they will be included in this array.
     *
     * @var array
     */
    protected $validations;

    /**
     * CountryService object.
     *
     * @var App\Services\CountryService
     */
    protected $countries;

    /**
     * TimeZoneService object.
     *
     * @var App\Services\TimeZoneService
     */
    protected $timeZones;

    /**
     * Class constructor.
     *
     * @param \Respect\Validation\Validator $validator
     * @param \App\Services\CountryService $country
     */
    public function __construct(Validator $validator, CountryService $country, TimeZoneService $timeZones)
    {
        $this->validator = $validator;
        $this->country = $country;
        $this->timeZone = $timeZones;
        $this->validations = [];
    }

    /**
     * Execute the validation logic.
     *
     * @param \App\Models\BaseModel $model
     *
     * @return App\Services\PhoneBookValidator
     */
    public function execute(BaseModel $model)
    {
        foreach ($model->rules() as $field => $rules) {
            foreach ($rules as $rule => $message) {
                $validation = $this->{$rule}($model->{$field});

                if (false == $validation) {
                    $this->validations[] = $message;
                }
            }
        }

        return $this;
    }

    /**
     * Verify if the validation failed.
     *
     * @return boolean
     */
    public function fail()
    {
        return (0 < count($this->validations));
    }

    /**
     * Verify if the validation passed.
     *
     * @return boolean
     */
    public function pass()
    {
        return (0 == count($this->validations));
    }

    /**
     * Get the validation messages from the validations array.
     *
     * @return array
     */
    public function messages()
    {
        return $this->validations;
    }

    /**
     * Validate required.
     *
     * @param string $input
     *
     * @return boolean
     */
    public function notEmpty($input)
    {
        return $this->validator->notEmpty()->validate($input);
    }

    /**
     * Validates a valid 7, 10, 11 digit phone number (North America, Europe and most Asian and Middle East countries),
     * supporting country and area codes (in dot, space or dashed notations) such as:
     *
     * (555)555-5555
     * 555 555 5555
     * +5(555)555.5555
     * 33(1)22 22 22 22
     * +33(1)22 22 22 22
     * +33(020)7777 7777
     * 03-6106666
     *
     * @param string $input
     *
     * @return boolean
     */
    public function phone($input)
    {
        return (empty($input)) ? true : $this->validator->phone()->validate($input);
    }

    /**
     * Validate the country code
     *
     * @param string $input
     *
     * @return boolean
     */
    public function countryCode($input)
    {
        return (empty($input)) ? true : in_array($input, $this->country->get());
    }

    /**
     * Validate time zone.
     *
     * @param string $input
     *
     * @return boolean
     */
    public function timeZone($input)
    {
        return (empty($input)) ? true : in_array($input, $this->timeZone->get());
    }
}
