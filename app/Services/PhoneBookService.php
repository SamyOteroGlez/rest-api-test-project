<?php

namespace App\Services;

use App\Models\PhoneBook;
use App\Services\PhoneBookValidator;
use League\Route\Http\Exception as HttpException;

class PhoneBookService
{
    /**
     * Phonebook object.
     *
     * @var App\Models\PhoneBook
     */
    protected $phonebook;

    /**
     * PoneBookValidator object.
     *
     * @var App\Services\PhoneBookValidator
     */
    protected $validator;

    /**
     * Class constructor.
     *
     * @param \App\Models\PhoneBook $phonebook
     */
    public function __construct(PhoneBook $phonebook, PhoneBookValidator $validator)
    {
        $this->phonebook = $phonebook;
        $this->validator = $validator;
    }

    /**
     * Get the phonebook.
     *
     * @return App\Models\PhoneBook
     */
    public function phonebook()
    {
        return $this->phonebook;
    }

    /**
     * Get all phonebooks.
     *
     * @param int $page
     *
     * @return array
     */
    public function all($page = null)
    {
        return $this->phonebook->paginate(null, ['*'], 'page', is_null($page) ? 1 : $page);
    }

    /**
     * FInd phonebook by id.
     *
     * @param int $id
     *
     * @return App\Models\PhoneBook
     */
    public function findById($id)
    {
        $phonebook = $this->phonebook->find($id);

        if ($phonebook) {
            return $phonebook;
        }

        throw new HttpException(404, 'PhoneBook not found');
    }

    /**
     * Find phonebook by first name.
     *
     * @param string $firstName
     * @param int $page
     *
     * @return array
     */
    public function findByFirstName($firstName, $page = null)
    {
        return $this->findLike('first_name', $firstName, $page);
    }

    /**
     * Find phonebook by last name.
     *
     * @param string $lastName
     * @param int $page
     *
     * @return array
     */
    public function findByLastName($lastName, $page = null)
    {
        return $this->findLike('last_name', $lastName, $page);
    }

    /**
     * Find phonebook by parameter match using LIKE.
     *
     * @param string $attribute
     * @param string $search
     * @param int $page
     *
     * @return array
     */
    public function findLike($attribute, $search, $page = null)
    {
        return $this->phonebook->where($attribute, 'like', "%$search%")
                    ->paginate(null, ['*'], 'page', is_null($page) ? 1 : $page);
    }

    /**
     * Create a phonebook.
     *
     * @param array $attributes
     *
     * @return App\Models\PhoneBook
     */
    public function create($attributes)
    {
        $this->phonebook->fill($attributes);
        $this->validator->execute($this->phonebook);

        if ($this->validator->fail()) {
            return $this->validator->messages();
        }

        $this->phonebook->save();

        return $this->phonebook;
    }

    /**
     * Update a phonebook.
     *
     * @param int $id
     * @param array $attributes
     *
     * @return App\Models\PhoneBook
     */
    public function update($id, $attributes)
    {
        $phonebook = $this->findById($id);

        if ($phonebook) {
            $phonebook->fill($attributes);
            $this->validator->execute($phonebook);

            if ($this->validator->fail()) {
                return $this->validator->messages();
            }

            $phonebook->save();
        }

        return $phonebook;
    }

    /**
     * Delete a phonebook.
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $phonebook = $this->findById($id);

        if ($phonebook) {
            $phonebook->delete();
        }
    }
}
