<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use App\Services\ExternalApiCallInterface;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Exception\BadResponseException;

class CountryService implements ExternalApiCallInterface
{
    /**
     * List of countries.
     *
     * @var array
     */
    protected $countries;

    /**
     * External endpoint url.
     *
     * @var string
     */
    protected $url;

    /**
     * Guzzle client object.
     *
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * Cache life time.
     *
     * @var int
     */
    protected $cacheTime;

    /**
     * Class constructor.
     *
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        $this->cacheTime = vars('cache-time');
        $this->url = vars('countries-endpoint');
        $this->client = $client;
    }

    /**
     * Set countries array.
     *
     * @param array $countries
     * @param boolean $keys
     *
     * @return App\Services\CountryService
     */
    public function setCountries($countries, $keys = true)
    {
        $this->countries = ($keys) ? array_keys($countries) : $countries;

        return $this;
    }

    /**
     * Get countries.
     *
     * @return array
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Get countries from the external source.
     *
     * @return array
     */
    public function get()
    {
        //check for cached list
        $cached = apcu_fetch('countries');

        if (false == $cached) {
            try {
                $response = $this->client->get($this->url);
                $contents = json_decode($response->getBody()->getContents(), true);
                $this->setCountries($contents['result']);

                //cache the list of countries
                apcu_store('countries', $this->countries, $this->cacheTime);
            } catch (ClientException | RequestException | ConnectException |
                        TransferException | BadResponseException | Exception $ex) {
                $class = get_class($ex);

                throw new $class($ex->getMessage(), $ex->getRequest());
            }
        } else {
            $this->countries = $cached;
        }

        return $this->countries;
    }
}
