<?php

namespace App\Models;

interface ModelInterface
{
    /**
     * Define a set of validation rules. The rule name needs to match with the validation rules
     * from respect/validation package.
     *
     * [
     *      'field-to-validate' => ['validation-rule' => 'Validation message'],
     * ]
     *
     * @return array
     */
    public function rules() : array;
}
