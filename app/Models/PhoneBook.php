<?php

namespace App\Models;

use App\Models\BaseModel;

class PhoneBook extends BaseModel
{
    /**
     * Table in the db.
     *
     * @var string
     */
    protected $table = 'phonebook';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['first_name', 'last_name', 'phone_number', 'country_code', 'time_zone'];

    /**
     * Define a set of validation rules. The rule name needs to match with the validation rules
     * from respect/validation package.
     *
     * [
     *      'field-to-validate' => ['validation-rule' => 'Validation message'],
     * ]
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'first_name' => [
                'notEmpty' => 'First name is required.'
            ],
            'phone_number' => [
                'notEmpty' => 'The phone number is required.',
                'phone' => 'The phone number is incorrect.'
            ],
            'country_code' => [
                'countryCode' => 'The provided country code is incorrect.'
            ],
            'time_zone' => [
                'timeZone' => 'The provided time zone is incorrect.'
            ],
        ];
    }
}
