<?php

namespace App\Controllers;

use Zend\Diactoros\Response;
use App\Services\PhoneBookService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class PhoneBookController
{
    /**
     * PhoneBookService object.
     *
     * @var App\Services\PhoneBookService
     */
    protected $service;

    /**
     * Class constructor.
     *
     * @param \App\Services\PhoneBookService $service
     */
    public function __construct(PhoneBookService $service)
    {
        $this->service = $service;
    }

    /**
     * Get all phonebooks.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return array
     */
    public function all(ServerRequestInterface $request, $attr) : array
    {
        return [
            'data' => $this->service->all($attr['page'])
        ];
    }

    /**
     * Find phonebook by id.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param array $attr
     *
     * @return array
     */
    public function findById(ServerRequestInterface $request, $attr) : array
    {
        return [
            'data' => $this->service->findById($attr['id'])
        ];
    }

    /**
     * Find phonebook by first name.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param array $attr
     *
     * @return array
     */
    public function findByFirstName(ServerRequestInterface $request, $attr) : array
    {
        return [
            'data' => $this->service->findByFirstName($attr['firstName'], $attr['page'])
        ];
    }

    /**
     * Find phonebook by last name.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param array $attr
     *
     * @return array
     */
    public function findByLastName(ServerRequestInterface $request, $attr) : array
    {
        return [
            'data' => $this->service->findByLastName($attr['lastName'], $attr['page'])
        ];
    }

    /**
     * Create a phonebook.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return array
     */
    public function create(ServerRequestInterface $request) : array
    {
        return [
            'data' => $this->service->create($request->getParsedBody())
        ];
    }

    /**
     * Update phonebook.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param array $attr
     *
     * @return array
     */
    public function update(ServerRequestInterface $request, $attr) : array
    {
        return [
            'data' => $this->service->update($attr['id'], $request->getParsedBody())
        ];
    }

    /**
     * Delete phonebook
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param array $attr
     *
     * @return array
     */
    public function delete(ServerRequestInterface $request, $attr) : array
    {
        return [
            'data' => $this->service->delete($attr['id'])
        ];
    }
}
