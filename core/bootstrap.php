<?php

/**
 * Create the application.
 *
 * First, a new instance of the application is created passing the application path as a parameter.
 */
$app = new Core\App\Application(realpath(__DIR__ . '/../'));

/**
 * Then lets return the application.
 */
return $app;
