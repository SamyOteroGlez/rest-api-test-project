<?php

namespace Core\App;

use League\Container\Container;
use League\Route\Strategy\JsonStrategy;
use Zend\Diactoros\ServerRequestFactory;

class Application
{
    /**
     * Application base path.
     *
     * @var string
     */
    protected $basePath;

    /**
     * Application configuration.
     *
     * @var array
     */
    protected $config;

    /**
     * Providers, an array of classes provided by the container.
     *
     * @var array
     */
    protected $providers;

    /**
     * Service providers array.
     *
     * @var array
     */
    protected $serviceProviders;

    /**
     * Application container.
     *
     * @var League\Container\Container
     */
    protected $container;

    /**
     * Router object;
     *
     * @var League\Route\Router
     */
    protected $router;

    /**
     * Request object;
     *
     * @var Zend\Diactoros\ServerRequest
     */
    protected $request;

    /**
     * Response object.
     *
     * @var Zend\Diactoros\Response
     */
    protected $response;

    /**
     * Emitter object
     *
     * @var Zend\Diactoros\Response\SapiEmitter
     */
    protected $emitter;


    /**
     * Database handler.
     *
     * @var Illuminate\Database\Capsule\Manager
     */
    protected $capsule;

    /**
     * Application class constructor.
     *
     * @param string $basePath
     */
    public function __construct($basePath = null)
    {
        if ($basePath) {
            $this->setBasePath($basePath);
        }

        $this->initApp();
    }

    /**
     * Init the application.
     *
     * @return void
     */
    protected function initApp()
    {
        //pre conditions
        $this->setContainer()
            ->loadConfig()

            //provide the app
            ->loadProviders()
            ->loadServiceProviders()
            ->provide()

            //db
            ->setCapsule()
            ->loadDbDriver()
            ->bootOrm()

            //routes, requests and responses
            ->setRouter()
            ->setRequest()
            ->setEmitter()
            ->routes()
        ;
    }

    /**
     * Set the db manager accessible in the application and boot Eloquent.
     *
     * @return Core\App\Application
     */
    protected function bootOrm()
    {
        $this->capsule->setAsGlobal();
        $this->capsule->bootEloquent();

        return $this;
    }

    /**
     * Load the db configuration.
     *
     * @return Core\App\Application
     */
    protected function loadDbDriver()
    {
        $this->capsule->addConnection($this->config['db']);

        return $this;
    }

    /**
     * Set the db capsule.
     *
     * @return Core\App\Application
     */
    protected function setCapsule()
    {
        $this->capsule = $this->get('Capsule');

        return $this;
    }

    /**
     * Load the application routes.
     *
     * @return Core\App\Application
     */
    protected function routes()
    {
        $responseFactory = $this->get('ResponseFactory');
        $strategy = new JsonStrategy($responseFactory);
        $strategy->setContainer($this->container);

        $this->router
            ->setStrategy($strategy)
            ->middleware($this->get('RouteMiddleware'))
            ->group('/', function (\League\Route\RouteGroup $route) {
                include $this->basePath('routes/routes.php');
            });

        return $this;
    }

    /**
     * Set the emitter.
     *
     * @return Core\App\Application
     */
    protected function setEmitter()
    {
        $this->emitter = $this->get('SapiEmitter');

        return $this;
    }

    /**
     * Set the request.
     *
     * @return Core\App\Application
     */
    protected function setRequest()
    {
        $this->request = ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);

        return $this;
    }

    /**
     * Set the router.
     *
     * @return Core\App\Application
     */
    protected function setRouter()
    {
        $this->router = $this->get('Router');

        return $this;
    }

    /**
     * Load the classes from the providers array into the container.
     *
     * @return Core\App\Application
     */
    protected function provide()
    {
        //providing core classes
        foreach ($this->providers as $alias => $provider) {
            $this->container->add($alias, $provider);
        }

        //providing service-providers
        foreach ($this->serviceProviders as $class => $argument) {

            if (!is_null($argument)) {
                $this->container->add($class)->addArguments($argument);
            } else {
                $this->container->add($class);
            }
        }

        return $this;
    }

    /**
     * Handle a request. Dispatch the request and then emit the response.
     *
     * @return void
     */
    public function handle()
    {
        $this->dispatchRequest()
            ->emitResponse();
    }

    /**
     * Emit the response.
     *
     * @return void
     */
    public function emitResponse()
    {
        $this->emitter->emit($this->response);
    }

    /**
     * Dispatch the request.
     *
     * @return Core\App\Application
     */
    public function dispatchRequest()
    {
        $this->response = $this->router->dispatch($this->request);

        return $this;
    }

    /**
     * Get a class from the application container.
     *
     * @param string $alias
     *
     * @return Object
     */
    public function get($alias)
    {
        return $this->container->get($alias);
    }

    /**
     * Set the application base path.
     *
     * @param string $basePath
     *
     * @return Core\App\Application
     */
    public function setBasePath($basePath)
    {
        $this->basePath = rtrim($basePath, '\/');

        return $this;
    }

    /**
     * Get an application path. Builds an return the bath appending the $path to the application basepath.
     *
     * @param string $path
     *
     * @return string
     */
    public function basePath($path = '')
    {
        return $this->basePath . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }

    /**
     * Load the configuration.
     *
     * @return Core\App\Application
     */
    protected function loadConfig()
    {
        if (file_exists($this->getConfigPath())) {
            $this->setConfig(require $this->getConfigPath());
        }

        return $this;
    }

    /**
     * Load the classes to be provided.
     *
     * @return Core\App\Application
     */
    protected function loadProviders()
    {
        $this->setProviders($this->config['providers']);

        return $this;
    }

    /**
     * Load the application service providers.
     *
     * @return Core\App\Application
     */
    protected function loadServiceProviders()
    {
        $this->setServiceProviders($this->config['service-providers']);

        return $this;
    }

    /**
     * Set the application configuration.
     *
     * @param array $config
     *
     * @return Core\App\Application
     */
    protected function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Set the application providers.
     *
     * @param array $providers
     *
     * @return Core\App\Application
     */
    protected function setProviders($providers)
    {
        $this->providers = $providers;

        return $this;
    }

    /**
     * Set the application service providers.
     *
     * @param array $serviceProviders
     *
     * @return Core\App\Application
     */
    protected function setServiceProviders($serviceProviders)
    {
        $this->serviceProviders = $serviceProviders;

        return $this;
    }

    /**
     * Get the application configuration path.
     *
     * @return string
     */
    protected function getConfigPath()
    {
        return $this->basePath . '/config/config.php';
    }

    /**
     * Create a new container instance.
     *
     * @return League\Container\Container
     */
    protected function newContainerInstance()
    {
        return new Container;
    }

    /**
     * Create a new json strategy instance.
     *
     * @param Http\Factory\Diactoros\ResponseFactory $responseFactory
     *
     * @return League\Route\Strategy\JsonStrategy
     */
    protected function newJsonStrategy($responseFactory)
    {
        return new JsonStrategy($responseFactory);
    }

    /**
     * Set the application container.
     *
     * @return Core\App\Application
     */
    protected function setContainer()
    {
        $this->container = $this->newContainerInstance();

        return $this;
    }
}
