<?php

if (!function_exists('vars')) {

    /**
     * Get attributes from the vars array.
     *
     * @param  string $key
     *
     * @return array
     */
    function vars($key = null)
    {
        $config = require __DIR__ . '/../../config/vars.php';

        if (is_null($key)) {
            return $config;
        }

        return $config[$key];
    }
}
