CREATE TABLE phonebook.phonebook (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT primary key,
	first_name varchar(100) NULL,
	last_name varchar(100) NULL,
	phone_number varchar(100) NULL,
	country_code varchar(100) NULL,
	time_zone varchar(100) NULL,
	created_at TIMESTAMP NULL,
	updated_at TIMESTAMP NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;
