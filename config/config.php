<?php

/**
 * Application configuration.
 */
return [

    /**
     * Classes that need to be injected into the application container
     */
    'providers' => [

        /**
         * Application core classes.
         */
        'ResponseFactory' => Http\Factory\Diactoros\ResponseFactory::class,
        'Router' => League\Route\Router::class,
        'SapiEmitter' => Zend\Diactoros\Response\SapiEmitter::class,
        'Capsule' => Illuminate\Database\Capsule\Manager::class,


        /**
         * Application core middlewares.
         */
        'RouteMiddleware' => Core\Middleware\RouteMiddleware::class,
    ],

    /**
     * Business logic classes to be provided.
     *
     * Class that needs to inject => [Argument to be injected]
     *
     * If no arguments needed then pass null.
     *
     * Class that needs to inject => null
     *
     * Example:
     * Class\Foo => [Class\Bar]
     *
     * class Foo {
     *      public function __construct(Class\Bar){};
     * }
     *
     * Class Bar {}
     */
    'service-providers' => [

        App\Controllers\PhoneBookController::class => [
            App\Services\PhoneBookService::class,
        ],

        App\Services\PhoneBookService::class => [
            App\Models\PhoneBook::class,
            App\Services\PhoneBookValidator::class,
        ],

        App\Models\PhoneBook::class => null,

        App\Services\PhoneBookValidator::class => [
            Respect\Validation\Validator::class,
            App\Services\CountryService::class,
            App\Services\TimeZoneService::class,
        ],

        Respect\Validation\Validator::class => null,

        App\Services\CountryService::class => [
            GuzzleHttp\Client::class,
        ],

        App\Services\TimeZoneService::class =>[
            GuzzleHttp\Client::class,
        ],

        GuzzleHttp\Client::class => null,
    ],

    /**
     * Db configuration.
     */
    'db' => [

        'driver'    => 'mysql',
        'host'      => 'db',
        'port'      => '3306',
        'database'  => 'phonebook',
        'username'  => 'root',
        'password'  => 'password',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ],
];
