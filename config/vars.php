<?php

return [

    /**
     * Cache time.
     * Defaults to 1 day => 86400 sec
     */
    'cache-time' => 86400,

    /**
     * Country list endpoint.
     * Used in App\Services\CountryService
     */
    'countries-endpoint' => 'https://api.hostaway.com/countries',

    /**
     * Timezone endpoint.
     * Used in App\Services\TimeZoneService
     */
    'timezones-endpoint' => 'https://api.hostaway.com/timezones',
];
