#
# Rest API test project
Public repo: `https://bitbucket.org/SamyOteroGlez/rest-api-test-project/src/master/`

## Requirements
- php 7.2
- mysql 5.5

## Docker requirements (optional)
- docker
- docker-compose

# Install

## Set up
- Clone or download the project.
- Execute `composer install`.

## Run with Docker
- Open a terminal in the root of the project and execute `make up`, this command will pull the docker images (if the first time), build and start the containers.

#### Note: It also runs with docker commands or docker-compose commands, for example, `docker-compose up -d` instead of `make up`.

## Or without docker
- Just like any other php web application.
- Don't forget the database.

## Database
- The db is located on `path-to-project/.docker/db/mysql`.
    * Server host: `localhost`
    * Port: `33066`
    * Database: `phonebook`
    * User name: `root`
    * Password: `password`
- If phonebook table doesn't exist, then execute the scrip.sql.
    * Path: `path-to-the-project/config/database/script.sql`
#### Note: if `path-to-project/.docker/db/mysql` doesn't exist it should be created automatically after the first build with docker. Yoy may need to grant proper right access to that folder on Unix systems.

## Other useful commands
- `make down`: stops the containers.
- `make destroy`: destroy the containers.
- `make shell`: shells into the container.

#### Note: For more commands check the `Makefile`

# The project

## API
- Base url: http://localhost:8006/api
- Get all: `/all`
- Get all with pagination: `/all/page={page}`
- Find by id: `/find/by/id/{id}`
- Find by first name: `/find/by/first/name/{firstName}`
- Find by first name wit pagination: `/find/by/first/name/{firstName}/page={page}`
- Find by last name: `/find/by/last/name/{lastName}`
- Find by last name with pagination: `/find/by/last/name/{lastName}/page={page}`
- Create: `/create`
- Update: `/update/{id}`
- Delete: `/delete/{id}`

#### Note: The endpoints `/find/by/first/name/{firstName}` and `/find/by/last/name/{lastName}` support partial search (LIKE %joe%).

## Validations
- First name: `required`.
- Phone number: `required` and validates a valid 7, 10, 11 digit phone number (North America, Europe and most Asian and Middle East countries) supporting country and area codes (in dot, space or dashed notations) such as:
    * (555)555-5555
    * 555 555 5555
    * +5(555)555.5555
    * 33(1)22 22 22 22
    * +33(1)22 22 22 22
    * +33(020)7777 7777
    * 03-6106666
- Country code: Validated via https://api.hostaway.com/countries, the validator checks if the country code exists.
- Timezone:  Validated via https://api.hostaway.com/timezones, the validator checks if the timezone exists.
- Inserted on (created_at): This value is saved automatically on every successful insert.
- Updated on (updated_at): This value is saved automatically on every successful update.

#### Note: The cached time for the country code and the timezone lists can be modified in `path-to-the-project/config/vars.php`.

## PHP packages
- league/route
- league/container
- zendframework/zend-diactoro
- http-interop/http-factory-diactoro
- illuminate/database
- respect/validation
- guzzlehttp/guzzle
- illuminate/pagination

#
## Todo
- Unit tests
- Integration tests

#### Note: Behat is my go-to test framework for php, the integration with phpunit is amazing and it is the official implementation of Cucumber for php.

#
## Extras
The project was successfully executed on:
- Ubuntu 18.04
- Docker v18.09.1
- Docker compose v1.17.1

Docker images:
- Database: `FROM mysql:5.5`
- Php: `FROM php:7.2-fpm-alpine`
- Web server: `FROM nginx:1.13-alpine`

#
## Final words
I really had lots of fun coding this project. Unfortunately, it was a one week project but I only had a weekend (partially). No regrets, I will definitely do it again.

## o(oO;)o
# Thanks
